﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;
using TaskDataAccess;

namespace Todo_Web.Controllers
{

    [EnableCors(origins: "*",headers: "*", methods: "*")]
    public class TasksController : ApiController
    {
        private TodolistEntities db = new TodolistEntities();

        /// <summary>
        /// Obtains tasks from the database given filtering and sorting query
        /// </summary>
        /// <param name="category">Category to search for</param>
        /// <param name="timeA">Earliest time for filter</param>
        /// <param name="timeB">Latest time for filter</param>
        /// <param name="sort">Sorting order</param>
        public IQueryable<Task> GetTasks(String category = null, String timeA = null, String timeB = null, String sort = null)
        {

            IQueryable<Task> result = null;
            DateTime earlyBound, lateBound;

            if ((timeA == null)||(timeA == "undefined")) // No earliest time given, set to 2009
            {
                timeA = "2009-12-31";
            }

            if ((timeB == null) || (timeB == "undefined")) // No latest time given, set to 2031
            {
                timeB = "2031-01-01";
            }

            // Set boundaries for time.

            earlyBound = DateTime.Parse(timeA);
            lateBound = DateTime.Parse(timeB);

            if (!((category == null) || (category == "undefined") || (category == "null"))) // Category was given, search for category
            {
                result = db.Tasks.Where
                (
                    (task) =>
                    (
                        (category == task.Category) &&  // Category Matches
                        (((earlyBound < task.Time) &&   // After timeA
                        (lateBound > task.Time)) ||     // After timeB
                        (task.Time == null))            // Time is NULL
                    )
                );
            }
            else // No category was given, omit matching
            {
                result = db.Tasks.Where
                (
                    (task) =>
                    (
                        ((earlyBound < task.Time) &&   // After timeA
                        (lateBound > task.Time)) ||    // After timeB
                        (task.Time == null)            // Time is NULL
                    )
                );
            }
            if (sort == "Latest First") // Order with latest date first
            {
                result = result.OrderByDescending
                (
                    (Task task) => (task.Time)
                );
            }
            else // Normal sorting, earliest date first
            {
                result = result.OrderBy
                (
                    (Task task) => (task.Time)
                );
            }
            return result;
        }


        /// <summary>
        /// Obtains a certain task from the database given its id
        /// </summary>
        /// <param name="id">id of the task to obtain</param>
        [ResponseType(typeof(Task))]
        public IHttpActionResult GetTask(int id)
        {
            Task task = db.Tasks.Find(id);
            if (task == null)
            {
                return NotFound();
            }

            return Ok(task);
        }

        /// <summary>
        /// Edits a task in the database
        /// </summary>
        /// <param name="id">id of the task to edit</param>
        /// <param name="task">task representing new data to place</param>
        [ResponseType(typeof(void))]
        public IHttpActionResult PutTask(int id, Task task)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != task.TaskID)
            {
                return BadRequest();
            }
           
            task.UpdatedAt = DateTime.Now;
            task.Time = (DateTime)task.Time;
            task.CreatedAt = (DateTime)task.Time;
            db.Entry(task).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!TaskExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        /// <summary>
        /// Adds a new task to the database
        /// </summary>
        /// <param name="task">Task to be added</param>
        [ResponseType(typeof(Task))]
        public IHttpActionResult PostTask(Task task)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            task.CreatedAt = DateTime.Now;
            task.UpdatedAt = DateTime.Now;
            db.Tasks.Add(task);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = task.TaskID }, task);
        }

        /// <summary>
        /// Deletes a task from the database
        /// </summary>
        /// <param name="id">id of task to be deleted</param>
        /// <returns></returns>
        [ResponseType(typeof(Task))]
        public IHttpActionResult DeleteTask(int id)
        {
            Task task = db.Tasks.Find(id);
            if (task == null)
            {
                return NotFound();
            }

            db.Tasks.Remove(task);
            db.SaveChanges();

            return Ok(task);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool TaskExists(int id)
        {
            return db.Tasks.Count(e => e.TaskID == id) > 0;
        }
    }
}