import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';

import { HomePage } from '../pages/home/home';
import { TasksPage } from '../pages/tasks/tasks'
import { TabsPage } from '../pages/tabs/tabs';
import { AddTaskPage } from '../pages/add-task/add-task'
import { EditTaskPage} from '../pages/edit-task/edit-task'
import { AddCategoryPage} from '../pages/add-category/add-category'
import { FilterPage } from '../pages/filter/filter'

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { TaskDbProvider } from '../providers/task-db/task-db';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    TasksPage,
    TabsPage,
    AddTaskPage,
    EditTaskPage,
    AddCategoryPage,
    FilterPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpClientModule,
    HttpModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    TasksPage,
    TabsPage,
    AddTaskPage,
    EditTaskPage,
    AddCategoryPage,
    FilterPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    TaskDbProvider
  ]
})
export class AppModule {}
