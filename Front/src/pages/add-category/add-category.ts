import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { TaskDbProvider } from '../../providers/task-db/task-db';
import { TasksPage } from '../tasks/tasks';

/**
 * Generated class for the AddCategoryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-add-category',
  templateUrl: 'add-category.html',
})
export class AddCategoryPage {

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController, public taskDbProvider: TaskDbProvider) {
  }

  /**
   * Category
   * 
   * Stores information about a category being created
   */
  category = {
    Name: ''
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddCategoryPage');
  }

  /**
   * discardCategory
   * 
   * Dismisses the category creation menu without creating a category
   */
  discardCategory(){
    this.viewCtrl.dismiss();
  }

  /**
   * saveCategory
   * 
   * Creates a category from the information provided and dismisses the menu
   */
  saveCategory(){
    console.log(this.category);
    this.taskDbProvider.addCategory(this.category).then
    (
      data => 
      {
        console.log('okay')
        this.viewCtrl.dismiss();
        this.navCtrl.setRoot(TasksPage);
      },
      
      err => 
      {
        console.log(err)
      }
    )
    this.viewCtrl.dismiss();
  }

}
