import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { TaskDbProvider } from '../../providers/task-db/task-db';
import { TasksPage } from '../tasks/tasks';

/**
 * Generated class for the AddTaskPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-add-task',
  templateUrl: 'add-task.html',
})
export class AddTaskPage {

  /**
   * Task
   * 
   * Stores information about a task being created
   */
  task = {
    Name: '',
    Category: null,
  }

  /**
   * Categories
   * 
   * Stores the possible categories a task can be assigned to
   */
  categories: any

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController, public taskDbProvider : TaskDbProvider) {
    this.taskDbProvider.getCategories().then
    ( 
      data => 
      {
      this.categories = data;
      },
    
      err =>
      {
        console.log(err);
      }
    );
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AddTaskPage');
  }

  /**
   * discardTask
   * 
   * Dismisses the task creation menu without creating a task
   */
  discardTask(){
    this.viewCtrl.dismiss();
  }

  /**
   * saveTask
   * 
   * Saves a task and dismisses the task creation menu
   */
  saveTask(){
    this.taskDbProvider.addTask(this.task).then
    (
      data => 
      {
        console.log('okay')
        this.navCtrl.setRoot(TasksPage); 
      },
      
      err => 
      {
        console.log(err)
      }
    )
    this.viewCtrl.dismiss();
  }

}
