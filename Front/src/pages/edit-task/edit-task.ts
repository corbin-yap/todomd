import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { TaskDbProvider } from '../../providers/task-db/task-db';
import { TasksPage } from '../../pages/tasks/tasks';

/**
 * Generated class for the EditTaskPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-edit-task',
  templateUrl: 'edit-task.html',
})
export class EditTaskPage {

  /**
   * Task
   * 
   * Holds information about the task being edited
   */
  task = 
  {
    Name: '',
    Time: null,
    Category: null,
    TaskID: null
  }

  /**
   * Categories
   * 
   * Holds the possible categories a task can be assigned to
   */
  categories: any

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController, public taskDbProvider: TaskDbProvider) { 
    this.taskDbProvider.getCategories().then
    ( 
      (data) => 
      {
        this.categories = data;
      },
    
      (err) =>
      {
        console.log(err);
      }
    );
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EditTaskPage');
    console.log(this.navParams.get('task'));
    console.log(this.navParams.get('categories'));
    this.categories = this.navParams.get('categories');
    this.task.Name = this.navParams.get('task').Name;
    this.task.Category = this.navParams.get('task').Category;
    this.task.Time = this.navParams.get('task').Time;
    this.task.TaskID = this.navParams.get('task').TaskID;
  }

  /**
   * discardTask
   * 
   * Dismisses the task editing menu without applying changes
   */
  discardTask(){
    this.viewCtrl.dismiss();
  }

  /**
   * saveTask
   * 
   * Saves the edited task to the database
   * @param id: ID of the task to modify
   */
  saveTask(id){
    this.taskDbProvider.editTask(this.task.TaskID, this.task).then
    (
      (data) => 
      {
        console.log('okay')
      },

      (err) => 
      {
        console.log(err)
      }
    )
    this.viewCtrl.dismiss();
    this.navCtrl.setRoot(TasksPage);
  }



}
