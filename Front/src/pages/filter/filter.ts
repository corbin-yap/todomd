import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';

/**
 * Generated class for the FilterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-filter',
  templateUrl: 'filter.html',
})
export class FilterPage {

  /**
   * Choices
   * 
   * Stores the choices made for the filter
   */
  choices: any = 
  {
    Category: null,
    Sorting: 'Latest First'
  }

  /**
   * Categories
   * 
   * Stores the possible categories that can be applied to filtering
   */
  categories: any

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FilterPage');
    this.categories = this.navParams.get('categories')
  } 

  /**
   * discardFilter
   * 
   * Dismisses the filter menu without applying a filter
   */
  discardFilter() {
    this.viewCtrl.dismiss(null);
  }
  
  /**
   * saveFilter
   * 
   * Sends information for a filter and dismisses the filter menu
   */
  saveFilter() {
    this.viewCtrl.dismiss(this.choices);
  }

}
