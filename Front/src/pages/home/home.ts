import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { TaskDbProvider } from '../../providers/task-db/task-db';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  /**
   * Count
   * 
   * Stores amount of tasks in the database
   */
  count: any = 0

  constructor(public navCtrl: NavController, public taskDbProvider: TaskDbProvider) 
  {
    this.taskDbProvider.getTasks().then
    (
      data =>
      {
        this.count = data;
        console.log(this.count.length);
      }
    );
  }

}
