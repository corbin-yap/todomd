import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { TaskDbProvider } from '../../providers/task-db/task-db';
import { AddTaskPage } from '../add-task/add-task';
import { EditTaskPage } from '../edit-task/edit-task';
import { AddCategoryPage } from '../add-category/add-category';
import { FilterPage } from '../filter/filter';

@IonicPage()
@Component({
  selector: 'page-tasks',
  templateUrl: 'tasks.html',
})
export class TasksPage {
  
  /**
   * SortMode
   * 
   * Stores mode of sorting
   */
  sortMode: any = 
  {
    order: 'ascend'
  }  

  /**
   * Tasks
   * 
   * Stores tasks of the database
   */
  tasks: any;

  /**
   * Categories
   * 
   * Stores categories of the database
   */
  categories: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public taskDbProvider: TaskDbProvider, public modalCtrl: ModalController) 
  {
    this.taskDbProvider.getTasks().then
    (
      (data) =>
      {
        this.tasks = data;
      }
    );
    
    this.taskDbProvider.getCategories().then
    (
      (data) =>
      {
        this.categories = data;
      }
    );  
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TasksPage');
  }

  /**
   * addNewTask
   * 
   * Handles adding of a new task
   */
  addNewTask()
  {
    this.navCtrl.push(AddTaskPage);
  }

  /**
   * addNewCategory
   * 
   * Handles adding of a new category
   */
  addNewCategory()
  {
    this.navCtrl.push(AddCategoryPage);
  }

  /**
   * editTask
   * 
   * Handles editing of an existing task
   * @param task: Task to be edited
   * @param categories: Categories in database 
   */
  editTask(task,categories)
  {
    let data = {
      task: task,
      categories: categories
    }
    this.navCtrl.push(EditTaskPage, data);
  }

  /**
   * deleteTask
   * 
   * Handles removal of a task from the database
   * @param task: Task to be deleted
   */
  deleteTask(task)
  {
    this.taskDbProvider.deleteTask(task.TaskID).then
    (
      (data) => 
      {
        console.log('okay')   
        this.navCtrl.setRoot(TasksPage); 
      },
      (err) => 
      {
        console.log(err)
      }
    )
  }

  /**
   * filter
   * 
   * Handles calling of filter method
   */
  filter()
  {
    let filterData = {
      categories: this.categories,
      sorting: this.sortMode
    }

    let modal = this.modalCtrl.create(FilterPage, filterData);

    modal.onDidDismiss((data) =>
    {
      if (data != null) // Filter data was set, perform filtering
      {
        console.log(data);
        this.applyFilters(data)
      }
    });

    modal.present()
  }

  /**
   * filter
   * 
   * Handles application of filters/sorting for tasks
   * @param data: Data related to filtering modes 
   */
  applyFilters(data)
  {

    this.taskDbProvider.getTasksByFilter(data.Category,data.DateA,data.DateB,data.Sorting).then(
      data => 
      {
        console.log(data);
        console.log('okay');       
        this.tasks = data;
      },

      err =>
      {
        console.log(err);
      }
    )
  }

}
