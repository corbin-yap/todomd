import { HttpClient } from '@angular/common/http';
import { HttpClientModule } from '@angular/common/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the TaskDbProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class TaskDbProvider {

  apiUrl = 'http://localhost:58014/api';

  constructor(public http: HttpClient) {
    console.log('Hello TaskDbProvider Provider');
  }

  /**
   * getTasks
   * 
   * Retrieves all tasks from the database
   */
  getTasks() 
  {
    return new Promise 
    (
      resolve => 
      {
        this.http.get(this.apiUrl+'/tasks').subscribe
        (
          (data) =>
          {
            console.log(data);
            resolve(data);
          }, 
        
          (err) => 
          {
            console.log(err);
          }
        );
      }
    );
  }

  /**
   * getTasksByFilter
   * 
   * Obtains a subset of the tasks in the data based on the passed filtering and sorting criteria
   * @param category: Determines the category to search for
   * @param dateTimeA: Determines the earliest time for the DateTime filter 
   * @param dateTimeB: Determines the latest time for the DateTime filter
   * @param sorting: Determines the sorting method for the tasks
   */
  getTasksByFilter(category, dateTimeA, dateTimeB, sorting)
  {
    console.log(category);
    console.log(dateTimeA);
    console.log(dateTimeB);
    console.log(sorting);

    return new Promise 
    (
      resolve =>
      {
        this.http.get(this.apiUrl+'/tasks?category='+category+'&timeA='+dateTimeA+'&timeB='+dateTimeB+'&sort='+sorting).subscribe
        (
          (data) =>
          {
            console.log(data);
            resolve(data);
          },
          
          (err) =>
          {
            console.log(err);
          }
        );
      }
    );
  }

  /**
   * getCategories
   * 
   * Retrieves all categories from the database
   */
  getCategories()
  {
    return new Promise 
    (
      resolve =>
      {
        this.http.get(this.apiUrl+'/categories').subscribe
        (
          (data) =>
          {
            console.log(data)
            resolve(data);
          },

          (err) =>
          {
            console.log(err);
          }
        );
      }
    );
  }

  /**
   * addCategory
   * 
   * Adds a category to the database
   * @param data: Data of the category to be added
   */
  addCategory(data)
  {
    return new Promise
    ( 
      (resolve, reject) =>
      {
        console.log(data)
        this.http.post(this.apiUrl+'/categories', data).subscribe
        (
          (res) =>
          {
            resolve(res);
          },
          (err) =>
          {
            reject(err);
          }
        );
      }
    );
  }

  /**
   * addTask
   * 
   * Adds a task to the database 
   * @param data: Data of the task to be added
   */
  addTask(data)
  {
    return new Promise
    (
      (resolve, reject) => 
      {
        this.http.post(this.apiUrl+'/tasks', data).subscribe
        (
          (res) => 
          {
            resolve(res);
          }, 
          
          (err) => 
          {
            reject(err);
          }
        );
      }
    );
  }

  /**
   * editTask
   * 
   * Edits a task in the database
   * @param id: ID of the task to be edited
   * @param data: Data of the edited task
   */
  editTask(id, data)
  {
    return new Promise
    (
      (resolve, reject) => 
      {
        this.http.put(this.apiUrl+'/tasks/'+id, data).subscribe
        (
          (res) => 
          {
            resolve(res);
          }, 
          
          (err) => 
          {
            reject(err);
          }
        );
      }
    );
  }

  /**
   * deleteTask
   * 
   * Deletes a task from the database
   * @param id: ID of the task to be removed
   */
  deleteTask(id)
  {
    return new Promise
    (
      (resolve, reject) =>
      {
        this.http.delete(this.apiUrl+'/tasks/'+id).subscribe
        (
          (res) => 
          {
            resolve(res);
          }, 
          
          (err) =>
          {
            reject(err);
          }
        );
      }
    );
  }


}
